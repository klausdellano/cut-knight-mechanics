extends Node

var time = 1.0

func _ready():
	var timer = Timer.new()
	timer.set_wait_time(time)
	timer.set_one_shot(true)
	timer.connect("timeout", self, "on_timeout")
	timer.start()
	add_child(timer)

func on_timeout():
	queue_free()