tool
extends Node2D

export var width = 50 setget set_width
export var height = 50 setget set_heigth

func _ready():
	
	pass

func _draw():
	get_node("frame").set_size(Vector2(width,height))
	get_node("frame").set_pos(Vector2((-width/2),(-height/2)))

func set_width(val):
	width = val
	update()

func set_heigth(val):
	height = val
	update()