tool
extends KinematicBody2D

var pre_coin = preload("res://scenes/phisic_coin.tscn")
var auto_destroy = preload("res://scripts/auto_destroy.gd")

export(AtlasTexture) var texture setget set_tex
export(int, 0, 20, 1) var moedas = 0
var divX = 4
var divY = 4

func _draw():
	get_node("sprite").set_texture(texture)
	pass

func destroy():
	moedas -= 1
	if moedas < 0:
		create_coin()
		var reg = get_node("sprite").get_texture().get_region()
		var tex = get_node("sprite").get_texture().get_atlas()
		var sizeX = reg.size.x/divX
		var sizeY = reg.size.y/divY
		
		for h in range(divY):
			for w in range(divX):
				var rec = Rect2(reg.pos.x + (sizeX * w), reg.pos.y + (sizeY * h), sizeX, sizeY)
				var spr = Sprite.new()
				spr.set_texture(tex)
				spr.set_region(true)
				spr.set_region_rect(rec)
				spr.set_centered(false)
				spr.set_global_pos(get_node("sprite").get_global_pos() + Vector2((sizeX * w),(sizeY * h)))
				var rig = RigidBody2D.new()
				rig.add_child(spr)
				rig.apply_impulse(Vector2(),Vector2(rand_range(-50,50),rand_range(-50,100)))
				rig.set_script(auto_destroy)
				rig.time = 3
				get_parent().add_child(rig)
		queue_free()
	else:
		create_coin()
		get_node("anim").play("shake")

func create_coin():
	var coin = pre_coin.instance()
	coin.set_global_pos(get_node("position").get_global_pos())
	coin.apply_impulse(Vector2(),Vector2(rand_range(-50,50),-70))
	get_parent().add_child(coin)

func set_tex(tex):
	texture = tex
	if get_tree() && get_tree().is_editor_hint():
		update()