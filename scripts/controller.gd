extends Node

export(NodePath) var player
var total_deaths = 0

func _ready():
	if player != null:
		player = get_node(player)
		player.connect("dead", self, "on_dead")
		get_node("UI/death/count_death").set_text(str(total_deaths))

func on_dead():
	total_deaths += 1
	get_node("UI/death/count_death").set_text(str(total_deaths))
	get_node("camera").set_global_pos(player.get_node("camera").get_camera_pos())
	get_node("camera").make_current()
	get_node("respaw_timer").start()

func _on_respaw_timer_timeout():
	player.respaw(get_node("respaw_point").get_global_pos())