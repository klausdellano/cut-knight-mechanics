extends Node

var credits_lbl = 0

func _ready():
	get_node("anim").play("intro")
	pass


func _on_timer_timeout():
	get_node("timer").start()
	
	if credits_lbl == 1:
		get_node("anim").play("programadores")
	elif credits_lbl == 2:
		get_node("anim").play("klaus")
	elif credits_lbl == 3:
		get_node("anim").play("lucas")
	elif credits_lbl == 4:
		get_node("anim").play("roteirista")
	elif credits_lbl == 5:
		get_node("anim").play("leticia")
	elif credits_lbl == 6:
		get_node("anim").play("design")
	elif credits_lbl == 7:
		get_node("anim").play("assets")
	
	credits_lbl += 1
	
	if credits_lbl > 7:
		credits_lbl = 0

func _on_btn_back_pressed():
	get_tree().change_scene("res://scenes/main.tscn")
