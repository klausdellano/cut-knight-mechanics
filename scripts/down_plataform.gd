extends KinematicBody2D

export var fall_time = 1.0
export var respaw_time = 2.0

var vel = 0

onready var init_pos = get_global_pos()

func _ready():
	get_node("fall_timer").set_wait_time(fall_time)
	get_node("respaw_timer").set_wait_time(respaw_time)

func _fixed_process(delta):
	translate(Vector2(0,1) * vel * delta)
	vel += 10

func _on_sensor_body_enter( body ):
	get_node("anim").play("shake")
	get_node("fall_timer").start()

func _on_sensor_body_exit( body ):
	get_node("anim").stop()
	get_node("fall_timer").stop()
	get_node("sprites").set_pos(Vector2())

func _on_fall_timer_timeout():
	get_node("respaw_timer").start()
	set_fixed_process(true)

func _on_respaw_timer_timeout():
	set_global_pos(init_pos)
	get_node("anim").stop()
	get_node("sprites").set_pos(Vector2())
	set_fixed_process(false)
	vel = 0
