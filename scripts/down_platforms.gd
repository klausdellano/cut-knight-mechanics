extends KinematicBody2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var vel = -16
var delt
var on_plat = false

var timer = true

func _ready():
	get_owner().get_node("player").connect("dead", self, "on_player_dead")
	set_process(true)

func on_player_dead():
	pass

func _process(delta):
	delt = delta
	if get_pos().y > 465:
		if on_plat:
			set_pos(get_pos() + Vector2(0, vel * delt))
	elif timer:
		get_node("back_timer").start()
		timer = false
	if !timer:
		get_node("back_timer").set_wait_time(2)
	

func _on_area_body_enter( body ):
	on_plat = true

func _on_back_timer_timeout():
	timer = true
	get_owner().get_node("down_platforms").set_global_pos(Vector2(728.992004,632.627991))