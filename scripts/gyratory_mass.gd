extends Node2D

export var vel = 50
export var radius = 50

var ang = 50

func _ready():
	set_fixed_process(true)

func _fixed_process(delta):
	ang += vel * delta
	get_node("mass").set_pos(Vector2(sin(deg2rad(ang)),cos(deg2rad(ang))) * radius)
	
	var radius_elo = 0
	
	for elo in get_node("chain").get_children():
		elo.set_pos(Vector2(sin(deg2rad(ang)),cos(deg2rad(ang))) * (radius / get_node("chain").get_child_count()) * radius_elo)
		radius_elo += 1

func _on_mass_body_enter( body ):
	body.death()
