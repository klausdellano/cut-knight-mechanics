extends Node

var pre_main = preload("res://scenes/main.tscn")
var pre_stage1 = preload("res://scenes/stage_1.tscn")
var pre_stage2 = preload("res://scenes/stage_2.tscn")
var pre_stage3 = preload("res://scenes/stage_3.tscn")
var game = 1
var game_now

func _ready():
	
	pass

func _on_btn_start_game_pressed():
	#game_now = 1
	#get_node("btn_start_game").hide()
	#get_node("background").hide()
	#new_game(game_now)
	new_game(game)
	
	

func new_game(val):
	#if game != null:
	#	game.queue_free()
	#if (game_now == 1):
	#	game = pre_stage1.instance()
	#elif (game_now == 2):
	#	game = pre_stage2.instance()
	#elif (game_now == 3):
	#	game = pre_stage3.instance()
	#add_child(game)
	#game.connect("next_stage", self, "on_next_stage")
	#if game == pre_stage3.instance():
	#	game.connect("back_game", self, "on_back_game")
	
	if val == 1:
		get_tree().change_scene("res://scenes/stage_1.tscn")
	elif val == 2:
		get_tree().change_scene("res://scenes/stage_2.tscn")
	elif val == 3:
		get_tree().change_scene("res://scenes/stage_3.tscn")
	elif val == 0:
		get_tree().change_scene("res://scenes/main.tscn")

func on_back_game():
	get_tree().change_scene("res://scenes/main.tscn")

func on_next_stage():
	game_now += 1
	new_game(game_now)

func _on_btn_credits_pressed():
	get_tree().change_scene("res://scenes/credits.tscn")
