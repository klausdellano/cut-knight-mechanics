extends Area2D

signal collected

func _ready():
	pass


func _on_moedas_body_enter( body ):
	if body.get_layer_mask() == 1 :
		get_node("shape").queue_free()
		get_node("anim").play("collect")
		yield(get_node("anim"), "finished")
		emit_signal("collected")
		queue_free()

