

extends KinematicBody2D

# This is a simple collision demo showing how
# the kinematic controller works.
# move() will allow to move the node, and will
# always move it to a non-colliding spot,
# as long as it starts from a non-colliding spot too.

# Member variables
const GRAVITY = 500.0 # Pixels/second

# Angle in degrees towards either side that the player can consider "floor"
const FLOOR_ANGLE_TOLERANCE = 40
const WALK_FORCE = 600
const WALK_MIN_SPEED = 10
const WALK_MAX_SPEED = 100
const STOP_FORCE = 1300
const JUMP_SPEED = 225
const JUMP_MAX_AIRBORNE_TIME = 0.2

const SLIDE_STOP_VELOCITY = 1.0 # One pixel per second
const SLIDE_STOP_MIN_TRAVEL = 1.0 # One pixel

var velocity = Vector2()
var on_air_time = 100
var jumping = false

var prev_jump_pressed = false

var new_anim = ""
var animation = ""
var was_floor = false
var push_force = Vector2(30,0)

var give_damage
var enter_body_enemy
var prev_atack = false
var can_atack = true

enum {LIVE, DEAD, WAIT}
var status = LIVE setget ,get_status
var coin = 0 setget , get_coin
var deaths = 0

signal dead
signal player_game_over


func _fixed_process(delta):
	if status == LIVE:
		moving(delta)
	elif status == DEAD:
		#dying(delta)
		pass

func moving(delta):
	# Create forces
	var force = Vector2(0, GRAVITY)
	
	var walk_left = Input.is_action_pressed("move_left")
	var walk_right = Input.is_action_pressed("move_right")
	var jump = Input.is_action_pressed("jump")
	var action = Input.is_action_pressed("action")
	var action2 = Input.is_action_pressed("teleport")
	var action3 = Input.is_action_pressed("float")
	
	var stop = true
	
	if (walk_left):
		if (velocity.x <= WALK_MIN_SPEED and velocity.x > -WALK_MAX_SPEED):
			force.x -= WALK_FORCE
			stop = false
	elif (walk_right):
		if (velocity.x >= -WALK_MIN_SPEED and velocity.x < WALK_MAX_SPEED):
			force.x += WALK_FORCE
			stop = false
	
	if (stop):
		var vsign = sign(velocity.x)
		var vlen = abs(velocity.x)
		
		vlen -= STOP_FORCE*delta
		if (vlen < 0):
			vlen = 0
		
		velocity.x = vlen*vsign
	
	# Integrate forces to velocity
	velocity += force*delta
	
	# Integrate velocity into motion and move
	var motion = velocity*delta
	
	# Move and consume motion
	motion = move(motion)
	
	var floor_velocity = Vector2()
	
	if (is_colliding()):
		# You can check which tile was collision against with this
		
		# Ran against something, is it the floor? Get normal
		var n = get_collision_normal()
		
		if (rad2deg(acos(n.dot(Vector2(0, -1)))) < FLOOR_ANGLE_TOLERANCE):
			# If angle to the "up" vectors is < angle tolerance
			# char is on floor
			on_air_time = 0
			floor_velocity = get_collider_velocity()
		
		if (on_air_time == 0 and force.x == 0 and get_travel().length() < SLIDE_STOP_MIN_TRAVEL and abs(velocity.x) < SLIDE_STOP_VELOCITY and get_collider_velocity() == Vector2()):
			# Since this formula will always slide the character around, 
			# a special case must be considered to to stop it from moving 
			# if standing on an inclined floor. Conditions are:
			# 1) Standing on floor (on_air_time == 0)
			# 2) Did not move more than one pixel (get_travel().length() < SLIDE_STOP_MIN_TRAVEL)
			# 3) Not moving horizontally (abs(velocity.x) < SLIDE_STOP_VELOCITY)
			# 4) Collider is not moving
			
			revert_motion()
			velocity.y = 0.0
		else:
			# For every other case of motion, our motion was interrupted.
			# Try to complete the motion by "sliding" by the normal
			motion = n.slide(motion)
			velocity = n.slide(velocity)
			# Then move again
			move(motion)
	
	if (floor_velocity != Vector2()):
		# If floor moves, move with floor
		move(floor_velocity*delta)
	
	if (jumping and velocity.y > 0):
		# If falling, no longer jumping
		jumping = false
	
	if (on_air_time < JUMP_MAX_AIRBORNE_TIME and jump and not prev_jump_pressed and not jumping):
		# Jump must also be allowed to happen if the character left the floor a little bit ago.
		# Makes controls more snappy.
		pula()
	
	on_air_time += delta
	prev_jump_pressed = jump

#############################################################################
	
	var _floor = (get_node("rayFloor").is_colliding() || get_node("rayFloor1"	).is_colliding())
	
	if _floor && !was_floor:
		get_node("animFX").play("fall")
	was_floor = _floor
	
	var walking = walk_right || walk_left
	
	if walking:
		if velocity.x > 0:
			#get_node("sprite").set_flip_h(false)
			get_node(".").set_global_scale(Vector2(1,1))
		else:
			#get_node("sprite").set_flip_h(true)
			get_node(".").set_global_scale(Vector2(-1,1))

	if _floor:
		if !jumping:
			velocity.y = 100
		if walking:
			new_anim = "walking"
			if action:
				atack()
		else:
			new_anim = "idle"
			if action:
				atack()
		
		get_node("ray_PushRight").set_enabled(walk_right)
		if get_node("ray_PushRight").is_colliding():
			var body = get_node("ray_PushRight").get_collider()
			body.move(push_force * delta)
		get_node("ray_PushLeft").set_enabled(walk_left)
		if get_node("ray_PushLeft").is_colliding():
			var body = get_node("ray_PushLeft").get_collider()
			body.move(push_force * -delta)
		
	else:
		if velocity.y < 0:
			new_anim = "jumpping"
		else:
			new_anim = "falling"
	
	if animation != new_anim:
		get_node("anim").play(new_anim)
		animation = new_anim
	
	give_damage = action and enter_body_enemy
	if give_damage:
		pass
	
	if action and !prev_atack:
		can_atack = false
		get_node("atack").start()
		get_node("UI_sword/sword/anim").play("reload")
	prev_atack = !can_atack

func _on_atack_timeout():
	can_atack = true

func atack():
	if can_atack:
		get_node("anim").play("atack")
		yield(get_node("anim"), "finished")

func _on_sword_body_enter( body ):
	enter_body_enemy = true
	if body.has_method("dano"):
		body.dano(1)

func _on_sword_body_exit( body ):
	enter_body_enemy = false

func _on_sword_area_enter( area ):
	if area.has_method("frame"):
		area.frame()

func pula():
	velocity.y = -JUMP_SPEED
	jumping = true
	get_node("animFX").play("jump")

var death_vel
var death_wt

func dying(delta):
	if death_wt > 0:
		death_wt -= delta
	else:
		move(death_vel * delta)
		death_vel += Vector2(0,2)

func death():
	if status == LIVE:
		deaths += 1
		status = DEAD
		get_node("shape").set_trigger(true)
		#death_vel = Vector2(0, -80)
		#death_wt = 1
		get_node("anim").play("death")
		emit_signal("dead")
		if deaths >= 20:
			emit_signal("player_game_over")

func player():
	pass

func respaw(pos):
	set_global_pos(pos)
	get_node("shape").set_trigger(false)
	status = LIVE
	get_node("camera").make_current()
	get_node("camera").reset_smoothing()

func get_status():
	return status

func _ready():
	randomize()
	set_fixed_process(true)
	if get_owner().has_node("coins"):
		for coin in get_owner().get_node("coins").get_children():
			coin.connect("collected", self, "on_collected")

func on_collected():
	coin += 1

func get_coin():
	return coin

func _on_feet_body_enter( body ):
	if body.has_method("dano"):
		pula()
		body.dano(1)


func _on_head_body_enter( body ):
	if velocity.y <= 0:
		if body.has_method("destroy"):
			body.destroy()