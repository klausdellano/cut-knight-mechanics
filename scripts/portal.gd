extends Node2D

export(int, 1,10) var coin = 1

signal next

func _on_scroll_body_enter( body ):
	if body.has_method("get_coin"):
		if body.get_coin() >= coin:
			body.status = 2
			get_node("anim").play("show_in")
			yield(get_node("anim"), "finished")
			body.status = 0
			emit_signal("next")