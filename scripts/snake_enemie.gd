extends KinematicBody2D

var pre_coin = preload("res://scenes/moedas.tscn")

export (int, 1, 10) var life = 1
var live = true

signal dead

func dano(value):
	life -= value
	if !live:
		return
	get_node("anim").play("hurt")
	yield(get_node("anim"), "finished")
	if life <= 0:
		if get_node("anim_2"):
			get_node("anim_2").stop(true)
		get_node("shape").queue_free()
		live = false
		emit_signal("dead")
		#get_node("area").queue_free()
		death()
	else:
		get_node("anim").play("walking")

func death():
	get_node("anim").play("death")
	yield(get_node("anim"),"finished")
	var coin = pre_coin.instance()
	coin.set_global_pos(get_global_pos())
	get_owner().add_child(coin)
	queue_free()
	pass

func _ready():
	pass