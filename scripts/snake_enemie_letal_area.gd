extends Area2D

func _ready():
	get_parent().connect("dead", self, "on_dead")

func on_dead():
	queue_free()

func _on_area_body_enter( body ):
	if body.get_layer_mask() == 1:
		body.death()
