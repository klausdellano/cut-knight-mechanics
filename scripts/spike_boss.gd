extends Node2D

export var tempo = 2

func _ready():
	get_node("timer").set_wait_time(tempo)

func moving():
	var rot = get_global_rotd()
	if rot == 90:
		side(2)
	else:
		side(-2)

func side(val):
	set_global_pos(Vector2((get_global_pos().x) - val,get_global_pos().y))

func _on_timer_timeout():
	moving()


func _on_area_body_enter( body ):
	if body.has_method("death"):
		body.death()
