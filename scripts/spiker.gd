extends Area2D


func _ready():
	get_parent().connect("enter",self,"on_enter")
	get_parent().connect("exit",self,"on_exit")


func _on_spiker_body_enter( body ):
	if body.has_method("death"):
		body.death()

func on_enter(obj):
	get_node("anim").play("show")

func on_exit(obj):
	get_node("anim").play("hide")