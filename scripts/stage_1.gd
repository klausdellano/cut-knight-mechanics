extends Node

var coins
var total_deaths = 0
var total_coin

signal next_stage


func _ready():
	total_coin = get_node("coins").get_children().size()
	get_node("UI_coins/count_coin").set_text("0" + "/" + str(total_coin))
	get_node("player").connect("dead", self, "on_player_death")
	get_node("portal").connect("next", self, "on_next_stage")
	coins = 0
	for coin in get_node("coins").get_children():
		coin.connect("collected", self, "on_collected")

func on_collected():
	coins += 1
	get_node("UI_coins/count_coin").set_text(str(coins) + "/" + str(total_coin))

func on_player_death():
	total_deaths += 1

func on_next_stage():
	#emit_signal("next_stage")
	get_tree().change_scene("res://scenes/stage_2.tscn")