tool
extends Area2D

export var width = 50 setget set_width
export var height = 50 setget set_heigth

onready var tween = get_node("tween")

enum{IDLE, SHOWING, HIDING}
var action = IDLE

signal showing
signal showed
signal hiding
signal hid

func _ready():
	if !get_tree().is_editor_hint():
		get_node("ballon").hide()

func _draw():
	get_node("ballon").width = width
	get_node("ballon").height = height

func _on_tip_body_enter( body ):
	if body.get_layer_mask() == 1:
		action = SHOWING
		tween.interpolate_method(self, "resize", 0, 1, 1.0, Tween.TRANS_BOUNCE, Tween.EASE_OUT, 0)
		tween.start()

func _on_tip_body_exit( body ):
	if body.get_layer_mask() == 1:
		get_node("timer").start()

func resize(val):
	if val >.1:
		get_node("ballon").show()
	else:
		get_node("ballon").hide()
	get_node("ballon").width = width * val
	get_node("ballon").height = height * val

func set_width(val):
	width = val
	update()

func set_heigth(val):
	height = val
	update()

func _on_timer_timeout():
	action = HIDING
	tween.interpolate_method(self, "resize", 1, 0, 0.5, Tween.TRANS_EXPO, Tween.EASE_OUT, 0)
	tween.start()


func _on_tween_tween_complete( object, key ):
	if action == HIDING:
		emit_signal("hid")
	elif action == SHOWING:
		emit_signal("showed")
	action == IDLE


func _on_tween_tween_start( object, key ):
	if action == HIDING:
		emit_signal("hiding")
	elif action == SHOWING:
		emit_signal("showing")
