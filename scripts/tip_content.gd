extends Node2D

signal showed
signal hiding

func _ready():
	hide()
	get_parent().connect("showed", self, "on_showed")
	get_parent().connect("hiding", self, "on_hiding")
	
func on_showed():
	emit_signal("showed")
	show()

func on_hiding():
	emit_signal("hiding")
	hide()