extends Label

export var wt = .05
var timer

func _ready():
	timer = Timer.new()
	timer.set_wait_time(wt)
	add_child(timer)
	timer.connect("timeout", self, "on_timeout")
	get_parent().connect("showed", self, "on_showed")
	get_parent().connect("hiding", self, "on_hiding")

func start():
	timer.start()

func on_timeout():
	set_visible_characters(get_visible_characters() + 1)
	if get_visible_characters() == get_total_character_count():
		timer.stop()

func on_showed():
	set_visible_characters(0)
	start()

func on_hiding():
	pass