extends KinematicBody2D

export (int, 1, 10) var life = 5
var live = true
var can_atack

var undy

var yet
var flip
var acerto

signal dead
signal hurt

enum {LIVE, DEAD, WAIT}
var status = LIVE

#------------------------------------

const GRAVITY = 500.0 # Pixels/second

# Angle in degrees towards either side that the player can consider "floor"
const FLOOR_ANGLE_TOLERANCE = 40
const WALK_FORCE = 600
const WALK_MIN_SPEED = 10
const WALK_MAX_SPEED = 100
const STOP_FORCE = 1300
const JUMP_SPEED = 225
const JUMP_MAX_AIRBORNE_TIME = 0.2

const SLIDE_STOP_VELOCITY = 1.0 # One pixel per second
const SLIDE_STOP_MIN_TRAVEL = 1.0 # One pixel

var velocity = Vector2()
var on_air_time = 100
var jumping = false

var prev_jump_pressed = false

var new_anim = ""
var animation = ""
var was_floor = false
var push_force = Vector2(30,0)

func moving(delta):
	# Create forces
	var force = Vector2(0, GRAVITY)
	
	var walk_left = false
	var walk_right = false
	var jump = false
	
	
	var stop = true
	
	if (walk_left):
		if (velocity.x <= WALK_MIN_SPEED and velocity.x > -WALK_MAX_SPEED):
			force.x -= WALK_FORCE
			stop = false
	elif (walk_right):
		if (velocity.x >= -WALK_MIN_SPEED and velocity.x < WALK_MAX_SPEED):
			force.x += WALK_FORCE
			stop = false
	
	if (stop):
		var vsign = sign(velocity.x)
		var vlen = abs(velocity.x)
		
		vlen -= STOP_FORCE*delta
		if (vlen < 0):
			vlen = 0
		
		velocity.x = vlen*vsign
	
	# Integrate forces to velocity
	velocity += force*delta
	
	# Integrate velocity into motion and move
	var motion = velocity*delta
	
	# Move and consume motion
	motion = move(motion)
	
	var floor_velocity = Vector2()
	
	if (is_colliding()):
		# You can check which tile was collision against with this
		# print(get_collider_metadata())
		
		# Ran against something, is it the floor? Get normal
		var n = get_collision_normal()
		
		if (rad2deg(acos(n.dot(Vector2(0, -1)))) < FLOOR_ANGLE_TOLERANCE):
			# If angle to the "up" vectors is < angle tolerance
			# char is on floor
			on_air_time = 0
			floor_velocity = get_collider_velocity()
		
		if (on_air_time == 0 and force.x == 0 and get_travel().length() < SLIDE_STOP_MIN_TRAVEL and abs(velocity.x) < SLIDE_STOP_VELOCITY and get_collider_velocity() == Vector2()):
			# Since this formula will always slide the character around, 
			# a special case must be considered to to stop it from moving 
			# if standing on an inclined floor. Conditions are:
			# 1) Standing on floor (on_air_time == 0)
			# 2) Did not move more than one pixel (get_travel().length() < SLIDE_STOP_MIN_TRAVEL)
			# 3) Not moving horizontally (abs(velocity.x) < SLIDE_STOP_VELOCITY)
			# 4) Collider is not moving
			
			revert_motion()
			velocity.y = 0.0
		else:
			# For every other case of motion, our motion was interrupted.
			# Try to complete the motion by "sliding" by the normal
			motion = n.slide(motion)
			velocity = n.slide(velocity)
			# Then move again
			move(motion)
	
	if (floor_velocity != Vector2()):
		# If floor moves, move with floor
		move(floor_velocity*delta)
	
	if (jumping and velocity.y > 0):
		# If falling, no longer jumping
		jumping = false
	
	#if (on_air_time < JUMP_MAX_AIRBORNE_TIME and jump and not prev_jump_pressed and not jumping):
		# Jump must also be allowed to happen if the character left the floor a little bit ago.
		# Makes controls more snappy.
	#	pula()
	
	on_air_time += delta
	prev_jump_pressed = jump

#------------------------------------

func _ready():
	undy = get_tree().get_root().get_node(".").get_canvas_transform().get_scale()
	set_process(true)

func _process(delta):
	if status == LIVE:
		moving(delta)
	can_atack = true

func dano(value):
	if status == LIVE:
		emit_signal("hurt")
		can_atack = false
		life -= value
		if !live:
			return
		get_node("anim").play("hurt")
		yield(get_node("anim"), "finished")
		grow()
		if life <= 0:
			get_node("area30").queue_free()
			#get_node("body").queue_free()
			live = false
			emit_signal("dead")
			death()
	else:
		pass
	can_atack = true

func grow():
	set_scale(Vector2((undy.x)+0.1,(undy.y)+0.1))
	undy = Vector2((undy.x)+0.1,(undy.y)+0.1)
	
	
func death():
	if status == LIVE:
		status = DEAD
		yield(get_node("anim"),"finished")
		get_node("body").queue_free()
		get_node("anim").play("dead")
		yield(get_node("anim"),"finished")
		get_node("timer_dead").start()

func _on_corpo_body_enter( body ):
	if status == LIVE:
		body.death()


func _on_area30_body_enter( body ):
	acerto = true
	if can_atack:
		get_node("anim").play("atack")
		yield(get_node("anim"), "finished")
		if acerto:
			body.death()
		get_node("anim").play("atack2")
		yield(get_node("anim"), "finished")

func _on_area30_body_exit( body ):
	acerto = false

func _on_timer_dead_timeout():
	queue_free()
