extends KinematicBody2D

var animation = -1
var anim = ["Forward","Forward","Forward"]

enum {LIVE, DEAD, WAIT}
var status = LIVE

signal game_over

func _ready():
	get_node("timer_atack").start()
	get_node("anim").play("idle")

func _on_area_body_enter( body ):
	if body.has_method("death"):
		body.death()

func _on_timer_atack_timeout():
	atack()

func atack():
	if status == LIVE:
		animation = rand_range(0,3)
		get_node("anim").play(anim[animation])
		yield(get_node("anim"), "finished")
		get_node("anim").play("idle")
		get_node("timer_atack").start()

func death():
	status = DEAD
	get_node("anim").play("dead")
	yield(get_node("anim"), "finished")
	emit_signal("game_over")
	
